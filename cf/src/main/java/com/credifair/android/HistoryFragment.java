package com.credifair.android;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.credifair.android.util.RestClient;
import com.credifair.android.util.TransactionAdapter;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.internal.ObjectConstructor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by indrek.ruubel on 24/05/2016.
 */
public class HistoryFragment extends Fragment {

	private RestClient restClient;

	private ListView transactionsList;

	private TransactionAdapter<LinkedTreeMap<String, Object>> transactionAdapter;

	public HistoryFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		restClient = new RestClient();
	}

	protected String getAccessTokenFromSharedPrefs() {
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
		return sharedPrefs.getString("accessToken", null);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.history_fragment, container, false);
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (isVisibleToUser) {
			// called here when view visible
			getHistory();
		}
	}

	private void getHistory() {
		restClient.transactions(getAccessTokenFromSharedPrefs(), new Callback<HashMap<String, Object>>() {
			@Override
			public void onResponse(Call<HashMap<String, Object>> call, Response<HashMap<String, Object>> response) {
				HashMap<String, Object> body = response.body();
				System.out.println(body);

				transactionAdapter.clear();

				List<LinkedTreeMap<String, Object>> transactions = (List<LinkedTreeMap<String, Object>>) body.get("transactions");
				for (LinkedTreeMap<String, Object> transaction : transactions) {
					transactionAdapter.add(transaction);
				}
			}

			@Override
			public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
				System.out.println(t.getMessage());
			}
		});
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		transactionsList = (ListView) view.findViewById(R.id.transactionsList);
		transactionAdapter = new TransactionAdapter<LinkedTreeMap<String, Object>>(getContext(), R.layout.transaction_spinner_item, new ArrayList<LinkedTreeMap<String, Object>>());
		transactionsList.setAdapter(transactionAdapter);
	}
}
