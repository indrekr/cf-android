package com.credifair.android;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import com.credifair.android.util.ExtendedActivity;
import com.credifair.android.util.RestClient;
import com.credifair.android.util.RouterService;
import com.credifair.android.util.Step;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.HashMap;

/**
 * Created by indrek.ruubel on 23/05/2016.
 */
public class BankDetails extends ExtendedActivity {

	private RestClient restClient;
	private RouterService routerService;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bankdetails);

		restClient = new RestClient();
		routerService = new RouterService();

		final EditText ibanInput = (EditText) findViewById(R.id.ibanInput);
		final EditText ukSortCodeInput = (EditText) findViewById(R.id.ukSortCodeInput);
		final EditText accountNumberInput = (EditText) findViewById(R.id.accountNumberInput);

		final Spinner bankTypeSpinner = (Spinner)findViewById(R.id.bankAccountTypeSpinner);
		String[] items = new String[]{"IBAN", "SORT"};
		final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items);
		bankTypeSpinner.setAdapter(adapter);

		bankTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				if (position == 0) {
					// Iban
					ibanInput.setVisibility(View.VISIBLE);
					ukSortCodeInput.setVisibility(View.INVISIBLE);
					accountNumberInput.setVisibility(View.INVISIBLE);
				} else {
					// Sort
					ibanInput.setVisibility(View.INVISIBLE);
					ukSortCodeInput.setVisibility(View.VISIBLE);
					accountNumberInput.setVisibility(View.VISIBLE);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});

		Button continueButton = (Button) findViewById(R.id.continueButton);

		continueButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				int position = bankTypeSpinner.getSelectedItemPosition();
				HashMap<String, Object> request = new HashMap<String, Object>();
				if (position == 0) {
					// Iban
					String iban = ibanInput.getText().toString();
					request.put("iban", iban);
				} else {
					// Sort
					String ukSort = ukSortCodeInput.getText().toString();
					String accountNumber = accountNumberInput.getText().toString();
					request.put("sort", ukSort);
					request.put("accountNumber", accountNumber);
				}

				restClient.postBankDetails(getAccessTokenFromSharedPrefs(), request, new Callback<HashMap<String, Object>>() {
					@Override
					public void onResponse(Call<HashMap<String, Object>> call, Response<HashMap<String, Object>> response) {
						HashMap<String, Object> body = response.body();
						String route = (String) body.get("goToStep");
						Step step = routerService.routeFromString(route);

						Intent k = new Intent(BankDetails.this, step.getClazz());
						startActivity(k);
						finish();
					}

					@Override
					public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
						System.out.println(t.getMessage());
						alertService.fail(BankDetails.this, t.getMessage());
					}
				});

			}
		});

	}

}
