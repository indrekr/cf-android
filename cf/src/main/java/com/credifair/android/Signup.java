package com.credifair.android;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.credifair.android.util.ExtendedActivity;
import com.credifair.android.util.RestClient;
import com.credifair.android.util.RouterService;
import com.credifair.android.util.Step;
import com.credifair.android.util.request.Country;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by indrek.ruubel on 16/05/2016.
 */
public class Signup extends ExtendedActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.signup);

		final Spinner countrySpinner = (Spinner)findViewById(R.id.country_spinner);
		String[] items = new String[]{Country.GB.getCountry(), Country.EE.getCountry()};
		final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items);
		countrySpinner.setAdapter(adapter);

		final EditText emailInput = (EditText) findViewById(R.id.email_input);
		final EditText passwordInput = (EditText) findViewById(R.id.password_input);

		final RestClient restClient = new RestClient();
		final RouterService routerService = new RouterService();

		Button signupButton = (Button) findViewById(R.id.signup_button);

		signupButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				final String email = emailInput.getText().toString().trim();
				final String password = passwordInput.getText().toString().trim();
				final String country = countrySpinner.getSelectedItem().toString();

				if (email.isEmpty() || password.isEmpty()) {
					return;
				}

				SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
				String clientToken = sharedPrefs.getString("clientToken", "");

				restClient.signup(email, password, country, clientToken, new Callback<HashMap<String, Object>>() {
					@Override
					public void onResponse(Call<HashMap<String, Object>> call, Response<HashMap<String, Object>> response) {
						restClient.getAccessToken(email, password, new Callback<HashMap<String, Object>>() {
							@Override
							public void onResponse(Call<HashMap<String, Object>> call, Response<HashMap<String, Object>> response) {
								HashMap<String, Object> body = response.body();
								String accessToken = (String) body.get("access_token");
								SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
								SharedPreferences.Editor editor = sharedPrefs.edit();
								editor.putString("accessToken", accessToken);
								editor.commit();
								restClient.getStep(accessToken, new Callback<HashMap<String, Object>>() {
									@Override
									public void onResponse(Call<HashMap<String, Object>> call, Response<HashMap<String, Object>> response) {
										HashMap<String, Object> body = response.body();
										String step = (String) body.get("goToStep");
										Step stepRoute = routerService.routeFromString(step);

										Intent k = new Intent(Signup.this, stepRoute.getClazz());
										startActivity(k);
										finish();
									}

									@Override
									public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
										System.out.println(t.getMessage());
									}
								});
							}

							@Override
							public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
								System.out.println(t.getMessage());
							}
						});
					}

					@Override
					public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
						System.out.println(t.getMessage());
						alertService.fail(Signup.this, "Sorry, but the inputs you specified are not correct");
					}
				});
			}
		});
	}

}
