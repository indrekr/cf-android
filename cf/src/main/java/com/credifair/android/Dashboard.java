package com.credifair.android;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.credifair.android.util.ExtendedAppCompatActivity;
import com.credifair.android.util.ExtendedSpinner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by indrek.ruubel on 22/05/2016.
 */
public class Dashboard extends ExtendedAppCompatActivity {

	private Toolbar toolbar;
	private TabLayout tabLayout;
	private ViewPager viewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dashboard);

		toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(false);

		viewPager = (ViewPager) findViewById(R.id.viewpager);
		setupViewPager(viewPager);

		tabLayout = (TabLayout) findViewById(R.id.tabs);
		tabLayout.setupWithViewPager(viewPager);

		Button navButton = (Button) findViewById(R.id.nav_button);
		final ExtendedSpinner navSpinner = (ExtendedSpinner) findViewById(R.id.spinner_nav);

		String[] items = new String[]{"Logout"};
		final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.navbar_spinner_item, R.id.itemName, items);
		navSpinner.setAdapter(adapter);

		// For some reason, onItemSelected is fired upon startup, so need a lock
		final int[] count = {0};
		navSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				if (count[0] > 0 && position == 0) {
					// Logout
					count[0] = 1;
					logout(Dashboard.this);
				}
				count[0]++;
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
		});

		navButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				navSpinner.performClick();
			}
		});
	}

	private void setupViewPager(ViewPager viewPager) {
		ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
		adapter.addFragment(new GetMoneyFragment(), "Get money");
		adapter.addFragment(new PaybackFragment(), "Pay back");
		adapter.addFragment(new HistoryFragment(), "History");
		viewPager.setAdapter(adapter);
	}

	class ViewPagerAdapter extends FragmentPagerAdapter {
		private final List<Fragment> mFragmentList = new ArrayList<Fragment>();
		private final List<String> mFragmentTitleList = new ArrayList<String>();

		public ViewPagerAdapter(FragmentManager manager) {
			super(manager);
		}

		@Override
		public Fragment getItem(int position) {
			return mFragmentList.get(position);
		}

		@Override
		public int getCount() {
			return mFragmentList.size();
		}

		public void addFragment(Fragment fragment, String title) {
			mFragmentList.add(fragment);
			mFragmentTitleList.add(title);
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return mFragmentTitleList.get(position);
		}
	}

}
