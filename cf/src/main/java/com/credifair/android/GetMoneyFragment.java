package com.credifair.android;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.credifair.android.util.AlertService;
import com.credifair.android.util.RestClient;
import com.google.gson.internal.LinkedTreeMap;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by indrek.ruubel on 24/05/2016.
 */
public class GetMoneyFragment extends Fragment {

	private RestClient restClient;
	private String accessToken;
	private AlertService alertService;

	private HashMap<String, Object> info;

	private EditText amountInput;
	private Button borrowButton;
	private TextView fullNameLabel;
	private TextView creditUsedLabel;
	private TextView aprLabel;
	private TextView creditLimitLiveLabel;
	private TextView bankDetailsLabel;

	private String currencySymbol;
	private String bankDetailsString;

	// The ISO/IEC 7810 standard dimensions ratio 53.98 : 85.60
	private Double creditCardHeightWidthRatio = 0.63060;

	public GetMoneyFragment() {
		// Required empty public constructor
	}

	protected String getAccessTokenFromSharedPrefs() {
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
		return sharedPrefs.getString("accessToken", null);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		accessToken = getAccessTokenFromSharedPrefs();
		restClient = new RestClient();
		alertService = new AlertService();
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);

		if (isVisibleToUser) {
			// called here
			updateUserInfo();
		}
	}

	private void updateUserInfo() {
		restClient.info(accessToken, new Callback<HashMap<String, Object>>() {
			@Override
			public void onResponse(Call<HashMap<String, Object>> call, Response<HashMap<String, Object>> response) {
				info = response.body();
				updateUiWithInfo(info);
				borrowButton.setEnabled(true);
			}
			@Override
			public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
				System.out.println(t.getMessage());
			}
		});
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.get_money_fragment, container, false);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		borrowButton = (Button) view.findViewById(R.id.borrowButton);
		borrowButton.setEnabled(false);
		fullNameLabel = (TextView) view.findViewById(R.id.fullNameLabel);
		amountInput = (EditText) view.findViewById(R.id.borrowAmountInput);
		creditUsedLabel = (TextView) view.findViewById(R.id.creditUsedLabel);
		aprLabel = (TextView) view.findViewById(R.id.aprLabel);
		creditLimitLiveLabel = (TextView) view.findViewById(R.id.creditLimitLiveLabel);
		bankDetailsLabel = (TextView) view.findViewById(R.id.bankDetailsLabel);

		final SurfaceView surfaceView = (SurfaceView) view.findViewById(R.id.creditCard);
		surfaceView.setZOrderOnTop(true);
		surfaceView.getHolder().setFormat(PixelFormat.TRANSLUCENT);

		// Set the real card height based on ratio in this context
		final RelativeLayout cardLayout = (RelativeLayout) view.findViewById(R.id.relativeLayout1);
		view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
			@Override
			public void onGlobalLayout() {
				int width = cardLayout.getWidth();
				int height = (int) (creditCardHeightWidthRatio * (double) width);
				cardLayout.getLayoutParams().height = height;
			}
		});

		borrowButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String amount = amountInput.getText().toString().trim();
				if (!amount.isEmpty()) {
					hideKeyboard();
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
					LinkedTreeMap<String, String> recipient = (LinkedTreeMap<String, String>) info.get("recipient");
					alertDialogBuilder.setTitle(String.format("Do you want to borrow %s%s?", amount, currencySymbol))
							.setMessage(generateRecipientAccountText(recipient))
							.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									final int amount = Integer.valueOf(amountInput.getText().toString());
									restClient.requestLoan(accessToken, amount, new Callback<HashMap<String, Object>>() {
										@Override
										public void onResponse(Call<HashMap<String, Object>> call, Response<HashMap<String, Object>> response) {
											Intent k = new Intent(getActivity(), SuccessLoanDialog.class);
											Bundle b = new Bundle();
											b.putInt("amount", amount);
											b.putString("currencySymbol", currencySymbol);
											b.putString("fullName", fullNameLabel.getText().toString());
											b.putString("bankDetails", bankDetailsString);
											k.putExtras(b);
											startActivity(k);
											getActivity().finish();
										}

										@Override
										public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
											alertService.fail(getActivity(), t.getMessage());
											clearAmountInput();
										}
									});
								}
							})
							.setNegativeButton("No", new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									// User cancelled the dialog
								}
							});
					// Create the AlertDialog object and return it
					alertDialogBuilder.create();
					alertDialogBuilder.show();
				}
			}
		});
	}

	private String generateRecipientAccountText(LinkedTreeMap<String, String> recipient) {
		String message = String.format("Money will be deposited to account:\n");
		message += generateBankDetailsString(recipient, true);
		return message;
	}

	private String generateBankDetailsString(LinkedTreeMap<String, String> recipient, boolean multiline) {
		if (recipient == null) {
			return "";
		}
		String message = "";
		String type = recipient.get("type");
		if (type.equals("IBAN")) {
			String iban = recipient.get("iban");
			message += String.format("%s: %s", type, iban);
		} else if(type.equals("SORT")) {
			String sort = recipient.get("sort");
			String accountNumber = recipient.get("accountNumber");
			message += String.format("UK Sort: %s%s", sort, multiline ? "\n" : " ");
			message += String.format("Account number: %s", accountNumber);
		}
		bankDetailsString = message;
		return message;
	}

	private void updateUiWithInfo(HashMap<String, Object> info) {
		System.out.println(info);

		// Full name
		String firstName = (String) info.get("firstName");
		String lastName = (String) info.get("lastName");
		fullNameLabel.setText(String.format("%s %s", firstName.toUpperCase(), lastName.toUpperCase()));


		// Update amount input hint with currency symbol
		currencySymbol = (String) info.get("currencySymbol");
		amountInput.setHint(String.format("Amount (%s)", currencySymbol));

		// Credit used
		String creditUsed = (String) info.get("loanBalanceText");
		creditUsedLabel.setText(creditUsed);

		// APR
		String aprText = (String) info.get("aprText");
		aprLabel.setText(String.format("%s", aprText));

		// creditLimitLive
		String creditLive = (String) info.get("creditLimitLiveText");
		creditLimitLiveLabel.setText(creditLive);

		// Bank details
		LinkedTreeMap<String, String> recipient = (LinkedTreeMap<String, String>) info.get("recipient");
		String account = generateBankDetailsString(recipient, false);
		bankDetailsLabel.setText(account);

	}

	private void hideKeyboard() {
		InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(amountInput.getWindowToken(), 0);
	}

	private void clearAmountInput() {
		amountInput.setText("");
	}

}
