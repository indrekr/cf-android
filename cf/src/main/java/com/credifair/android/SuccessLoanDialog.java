package com.credifair.android;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.credifair.android.util.ExtendedActivity;

/**
 * Created by indrek.ruubel on 25/05/2016.
 */

public class SuccessLoanDialog extends ExtendedActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.success_loan_dialog);

		TextView amountLabel = (TextView) findViewById(R.id.amountLabel);
		TextView fullNameLabel = (TextView) findViewById(R.id.fullNameLabel);
		TextView bankDetailsLabel = (TextView) findViewById(R.id.bankDetailsLabel);
		Button okayButton = (Button) findViewById(R.id.okayButton);

		Bundle b = getIntent().getExtras();
		int amount = b.getInt("amount");
		String currencySymbol = b.getString("currencySymbol");
		String fullName = b.getString("fullName");
		String bankDetails = b.getString("bankDetails");

		amountLabel.setText(String.format("%s %s", amount, currencySymbol));
		fullNameLabel.setText(fullName);
		bankDetailsLabel.setText(bankDetails);

		okayButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent k = new Intent(SuccessLoanDialog.this, Dashboard.class);
				startActivity(k);
			}
		});
	}

}
