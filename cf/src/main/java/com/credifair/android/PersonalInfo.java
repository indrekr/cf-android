package com.credifair.android;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.credifair.android.util.AlertService;
import com.credifair.android.util.ExtendedActivity;
import com.credifair.android.util.RestClient;
import com.credifair.android.util.RouterService;
import com.credifair.android.util.Step;
import com.credifair.android.util.request.Education;
import com.credifair.android.util.request.MaritalStatus;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by indrek.ruubel on 22/05/2016.
 */
public class PersonalInfo extends ExtendedActivity {

	DatePickerDialog datePickerDialog;
	private RestClient restClient;
	private RouterService routerService;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.personal_info);

		final EditText firstName = (EditText) findViewById(R.id.firstName);
		final EditText lastName = (EditText) findViewById(R.id.lastName);
		final EditText phone = (EditText) findViewById(R.id.phone);
		Button pickDateButton = (Button) findViewById(R.id.pickDateButton);

		restClient = new RestClient();
		routerService = new RouterService();

		final TextView dobInput = (TextView) findViewById(R.id.dob);
		dobInput.setEnabled(false);

		// Education
		final Spinner educationSpinner = (Spinner)findViewById(R.id.educationSpinner);
		String[] items = new String[]{Education.BASIC.getLabel(),
				Education.PRIMARY.getLabel(),
				Education.SECONDARY.getLabel(),
				Education.HIGHER.getLabel()};
		final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items);
		educationSpinner.setAdapter(adapter);

		// Marital status
		final Spinner maritalStatusSpinner = (Spinner)findViewById(R.id.maritalStatus);
		String[] items2 = new String[]{MaritalStatus.SINGLE.getLabel(),
		MaritalStatus.COHABITANT.getLabel(), MaritalStatus.MARRIED.getLabel(),
		MaritalStatus.DIVORCED.getLabel(), MaritalStatus.WIDOW.getLabel()};
		final ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items2);
		maritalStatusSpinner.setAdapter(adapter2);

		pickDateButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// calender class's instance and get current date , month and year from calender
				final Calendar c = Calendar.getInstance();
				int mYear = c.get(Calendar.YEAR); // current year
				int mMonth = c.get(Calendar.MONTH); // current month
				int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
				// date picker dialog
				datePickerDialog = new DatePickerDialog(PersonalInfo.this,
						new DatePickerDialog.OnDateSetListener() {
							@Override
							public void onDateSet(DatePicker view, int year,
												  int monthOfYear, int dayOfMonth) {
								// set day of month , month and year value in the edit text
								dobInput.setText(dayOfMonth + "/"
										+ (monthOfYear + 1) + "/" + year);
							}
						}, mYear, mMonth, mDay);
				datePickerDialog.show();
			}
		});

		Button continueButton = (Button) findViewById(R.id.continueButton);

		continueButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String firstNameText = firstName.getText().toString();
				String lastNameText = lastName.getText().toString();
				String dob = dobInput.getText().toString();
				String phoneText = phone.getText().toString();

				String educationLabel = educationSpinner.getSelectedItem().toString();
				String maritalStatusLabel = maritalStatusSpinner.getSelectedItem().toString();
				Education education = Education.findByLabel(educationLabel);
				MaritalStatus maritalStatus = MaritalStatus.findByLabel(maritalStatusLabel);

				SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
				Date date = null;
				try {
					date = format.parse(dob);
				} catch (ParseException e) {
					e.printStackTrace();
				}

				if (date != null) {
					TimeZone tz = TimeZone.getTimeZone("UTC");
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZZZZZ");
					df.setTimeZone(tz);
					dob = (df.format(date)).trim();
				}

				HashMap<String, Object> request = new HashMap<String, Object>();

				request.put("FIRST_NAME", firstNameText);
				request.put("LAST_NAME", lastNameText);
				request.put("DATE_OF_BIRTH", dob);
				request.put("PHONE_NUMBER", phoneText);
				request.put("EDUCATION", education);
				request.put("MARITAL_STATUS", maritalStatus);

				String accessToken = getAccessTokenFromSharedPrefs();

				restClient.putData(accessToken, request, new Callback<HashMap<String, Object>>() {
					@Override
					public void onResponse(Call<HashMap<String, Object>> call, Response<HashMap<String, Object>> response) {
						HashMap<String, Object> body = response.body();
						String route = (String) body.get("goToStep");
						Step step = routerService.routeFromString(route);

						Intent k = new Intent(PersonalInfo.this, step.getClazz());
						startActivity(k);
						finish();
					}

					@Override
					public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
						System.out.println(t.getMessage());
						alertService.fail(PersonalInfo.this, t.getMessage());
					}
				});

			}
		});

	}

}
