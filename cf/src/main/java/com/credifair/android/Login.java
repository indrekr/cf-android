package com.credifair.android;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.credifair.android.util.*;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.HashMap;

/**
 * Created by indrek.ruubel on 21/05/2016.
 */
public class Login extends ExtendedActivity {

	private RestClient restClient;
	private RouterService routerService;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);

		restClient = new RestClient();
		routerService = new RouterService();

		final EditText emailInput = (EditText) findViewById(R.id.email_input);
		final EditText passwordInput = (EditText) findViewById(R.id.password_input);
		Button loginButton = (Button) findViewById(R.id.login_button);

		loginButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				final String email = emailInput.getText().toString().trim();
				final String password = passwordInput.getText().toString().trim();

				if (email.isEmpty() || password.isEmpty()){
					return;
				}

				restClient.getAccessToken(email, password, new Callback<HashMap<String, Object>>() {
					@Override
					public void onResponse(Call<HashMap<String, Object>> call, Response<HashMap<String, Object>> response) {
						HashMap<String, Object> body = response.body();
						String accessToken = (String) body.get("access_token");
						SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
						SharedPreferences.Editor editor = sharedPrefs.edit();
						editor.putString("accessToken", accessToken);
						editor.commit();
						restClient.getStep(accessToken, new Callback<HashMap<String, Object>>() {
							@Override
							public void onResponse(Call<HashMap<String, Object>> call, Response<HashMap<String, Object>> response) {
								HashMap<String, Object> body = response.body();
								String step = (String) body.get("goToStep");
								Step stepRoute = routerService.routeFromString(step);

								Intent k = new Intent(Login.this, stepRoute.getClazz());
								startActivity(k);
								finish();
							}

							@Override
							public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {

							}
						});
					}

					@Override
					public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
						alertService.fail(Login.this, "Wrong e-mail or password");
						passwordInput.setText("");
					}
				});
			}
		});
	}
}
