package com.credifair.android.util.api;

import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

import java.util.HashMap;

/**
 * Created by indrek.ruubel on 16/05/2016.
 */
public interface ApiMarketingService {

	@POST("api/v1/marketing/subscribe/{email}")
	Call<HashMap<String, Object>> subscribe(@Header("Authorization") String authorization, @Path("email") String email);
}
