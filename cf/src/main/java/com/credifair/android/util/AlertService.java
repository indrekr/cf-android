package com.credifair.android.util;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

/**
 * Created by indrek.ruubel on 25/05/2016.
 */
public class AlertService {

	public void success(Context context, String message) {
		triggerAlert(context, "Done!", message);
	}

	public void fail(Context context, String message) {
		triggerAlert(context, "Oops! Something went wrong", message);
	}

	private void triggerAlert(Context context, String title, String message) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		AlertDialog dialog = null;
		alertDialogBuilder.setTitle(title)
		.setMessage(message)
		.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
			}
		});
		dialog = alertDialogBuilder.create();
		dialog.show();
	}

}
