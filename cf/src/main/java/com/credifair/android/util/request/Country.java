package com.credifair.android.util.request;

/**
 * Created by indrek.ruubel on 22/05/2016.
 */
public enum Country {

	EE("Estonia", "EE", "est", 372, "233"),
	US("United States", "US", "usa", 1, "840"),
	GB("United Kingdom", "GB", "gbr", 44, "826");

	private String country;
	private String countryISO2code; // ISO3166_2 a.k.a ISO 2-alpha code
	private String countryISO3code; // ISO3166_3 a.k.a ISO 3-alpha code
	private Integer callingCode;
	private String numeric3Code; // ISO3166_1 a.k.a numeric-3

	Country(String country, String countryISO2code, String countryISO3code, Integer callingCode, String numeric3Code) {
		this.country = country;
		this.countryISO2code = countryISO2code;
		this.countryISO3code = countryISO3code;
		this.callingCode = callingCode;
		this.numeric3Code = numeric3Code;
	}

	public String getCountry() {
		return country;
	}

	public String getCountryISO2code() {
		return countryISO2code;
	}

	public String getCountryISO3code() {
		return countryISO3code;
	}

	public Integer getCallingCode() {
		return callingCode;
	}

	public String getNumeric3Code() {
		return numeric3Code;
	}

	public static Country findByCountry(String country) {
		for (Country country1 : values()) {
			if (country1.getCountry().equals(country)) {
				return country1;
			}
		}
		return null;
	}

}
