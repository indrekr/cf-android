package com.credifair.android.util;

import com.credifair.android.*;

/**
 * Created by indrek.ruubel on 22/05/2016.
 */
public enum Step {
	MAIN(Main.class),
	SIGNUP(Signup.class),
	LOGIN(Login.class),
	WAITING_PROCESSING(Processing.class),
	STEP1_PERSONAL_INFO(PersonalInfo.class),
	STEP2_ADDRESS(Address.class),
	STEP3_ID_UPLOAD(UploadId.class),
	STEP4_BANK_DETAILS(BankDetails.class),
	DASHBOARD(Dashboard.class);

	Class clazz;

	Step(Class clazz) {
		this.clazz = clazz;
	}

	public Class getClazz() {
		return clazz;
	}
}


