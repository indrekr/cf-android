package com.credifair.android.util;

/**
 * Created by indrek.ruubel on 22/05/2016.
 */
public class RouterService {

	public Step routeFromString(String route) {
		Step step = Step.WAITING_PROCESSING;
		System.out.println("Searching route: " + route);
		try {
			step = Step.valueOf(route);
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
		return step;
	}

}
