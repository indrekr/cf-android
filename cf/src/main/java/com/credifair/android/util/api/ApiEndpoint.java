package com.credifair.android.util.api;

/**
 * Created by indrek.ruubel on 21/05/2016.
 */
public enum ApiEndpoint {
	API_URL("http://credifair.com:8080/");

	private String value;

	ApiEndpoint(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
