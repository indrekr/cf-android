package com.credifair.android.util;

import android.content.Context;
import android.net.Uri;
import android.util.Base64;
import com.credifair.android.util.api.ApiCustomerService;
import com.credifair.android.util.api.ApiEndpoint;
import com.credifair.android.util.request.Country;
import com.credifair.android.util.request.SignupRequest;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;

/**
 * Created by indrek.ruubel on 22/05/2016.
 */
public class RestClient {

	public final static String API_CLIENT_USERNAME = "api-client";
	public final static String API_CLIENT_PASSWORD = "12345";

	private Retrofit retrofit;
	private String clientAuthorization;
	private RouterService routerService;

	private ApiCustomerService apiCustomerService;

	public RestClient() {

		retrofit = new Retrofit.Builder()
				.baseUrl(ApiEndpoint.API_URL.getValue())
				.addConverterFactory(GsonConverterFactory.create())
				.build();

		apiCustomerService = retrofit.create(ApiCustomerService.class);

		routerService = new RouterService();
		buildClientB64Basic();
	}

	private void buildClientB64Basic() {
		String authorizationRaw = API_CLIENT_USERNAME + ":" + API_CLIENT_PASSWORD;
		byte[] data = new byte[0];
		try {
			data = authorizationRaw.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		String authorization = Base64.encodeToString(data, Base64.DEFAULT);
		clientAuthorization = ("Basic " + authorization).trim();
	}

	public void getClientToken(final Callback<HashMap<String, Object>> callback) {
		Call<HashMap<String, Object>> call = apiCustomerService.getClientToken(clientAuthorization);
		call.enqueue(callback(callback));
	}

	public void signup(String username, String password, String country, String clientToken, final Callback<HashMap<String, Object>> callback) {
		Country countryEnum = Country.findByCountry(country);
		SignupRequest request = new SignupRequest(username, password, countryEnum);
		Call<HashMap<String, Object>> call = apiCustomerService.signup(("Bearer " + clientToken).trim(), request);
		call.enqueue(callback(callback));
	}

	public void getAccessToken(String username, String password, final Callback<HashMap<String, Object>> callback) {
		Call<HashMap<String, Object>> call =
				apiCustomerService.getAccessToken(clientAuthorization, username, password, "password", "read write");
		call.enqueue(callback(callback));
	}

	public void getStep(String accessToken, final Callback<HashMap<String, Object>> callback) {
		Call<HashMap<String, Object>> call =
				apiCustomerService.getStep(("Bearer " + accessToken).trim());
		call.enqueue(callback(callback));
	}

	public void putData(String accessToken, HashMap<String, Object> request, final Callback<HashMap<String, Object>> callback) {
		Call<HashMap<String, Object>> call = apiCustomerService.putData(("Bearer " + accessToken).trim(), request);
		call.enqueue(callback(callback));
	}

	public void upload(String accessToken, Context context, Uri fileUri, final Callback<HashMap<String, Object>> callback) {
		File file = FileUtils.getFile(context, fileUri);
		// create RequestBody instance from file
		RequestBody requestFile =
				RequestBody.create(MediaType.parse("multipart/form-data"), file);

		// MultipartBody.Part is used to send also the actual file name
		MultipartBody.Part body =
				MultipartBody.Part.createFormData("file", file.getName(), requestFile);

		Call<HashMap<String, Object>> call = apiCustomerService.upload(("Bearer " + accessToken).trim(), body);
		call.enqueue(callback(callback));
	}

	public void idDocsUploaded(String accessToken, final Callback<HashMap<String, Object>> callback) {
		Call<HashMap<String, Object>> call = apiCustomerService.idDocsUploaded(("Bearer " + accessToken).trim());
		call.enqueue(callback(callback));
	}

	public void postBankDetails(String accessToken, HashMap<String, Object> request, final Callback<HashMap<String, Object>> callback) {
		Call<HashMap<String, Object>> call = apiCustomerService.postBankDetails(("Bearer " + accessToken).trim(), request);
		call.enqueue(callback(callback));
	}

	public void info(String accessToken, final Callback<HashMap<String, Object>> callback) {
		Call<HashMap<String, Object>> call = apiCustomerService.info(("Bearer " + accessToken).trim());
		call.enqueue(callback(callback));
	}

	public void requestLoan(String accessToken, int amount, final Callback<HashMap<String, Object>> callback) {
		Call<HashMap<String, Object>> call = apiCustomerService.requestLoan(("Bearer " + accessToken).trim(), amount);
		call.enqueue(callback(callback));
	}

	public void payoffDetails(String accessToken, final Callback<HashMap<String, Object>> callback) {
		Call<HashMap<String, Object>> call = apiCustomerService.payOffDetails(("Bearer " + accessToken).trim());
		call.enqueue(callback(callback));
	}

	public void payoffSchedule(String accessToken, final Callback<HashMap<String, Object>> callback) {
		Call<HashMap<String, Object>> call = apiCustomerService.payOffSchedule(("Bearer " + accessToken).trim());
		call.enqueue(callback(callback));
	}

	public void transactions(String accessToken, final Callback<HashMap<String, Object>> callback) {
		Call<HashMap<String, Object>> call = apiCustomerService.transactions(("Bearer " + accessToken).trim());
		call.enqueue(callback(callback));
	}

	private Callback<HashMap<String, Object>> callback(final Callback callback) {
		return new Callback<HashMap<String, Object>>() {
			@Override
			public void onResponse(Call<HashMap<String, Object>> call, Response<HashMap<String, Object>> response) {
				if (response.isSuccessful()) {
					callback.onResponse(call, response);
				} else {
					String error = null;
					try {
						error = response.errorBody().string();
					} catch (IOException e) {
						e.printStackTrace();
					}
					callback.onFailure(call, new Throwable(error));
				}
			}
			@Override
			public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
				callback.onFailure(call, t);
			}
		};
	}

}
