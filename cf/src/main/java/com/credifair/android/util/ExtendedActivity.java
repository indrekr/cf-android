package com.credifair.android.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by indrek.ruubel on 22/05/2016.
 */
public class ExtendedActivity extends Activity {

	protected AlertService alertService;

	public ExtendedActivity() {
		alertService = new AlertService();
	}

	protected String getAccessTokenFromSharedPrefs() {
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		return sharedPrefs.getString("accessToken", null);
	}

	protected void removeAccessTokenFromSharedPrefs() {
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		SharedPreferences.Editor edit = sharedPrefs.edit();
		edit.remove("accessToken");
		edit.commit();
	}

	protected void logout(Context context) {
		removeAccessTokenFromSharedPrefs();
		Intent k = new Intent(context, Step.MAIN.getClazz());
		startActivity(k);
	}

}
