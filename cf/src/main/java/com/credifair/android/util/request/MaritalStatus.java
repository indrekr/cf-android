package com.credifair.android.util.request;

/**
 * Created by indrek.ruubel on 22/05/2016.
 */
public enum MaritalStatus {

	MARRIED("Married"), COHABITANT("Cohabitant"), SINGLE("Single"), DIVORCED("Divorced"), WIDOW("Widow");

	private String label;

	MaritalStatus(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public static MaritalStatus findByLabel(String label) {
		for (MaritalStatus marital : values()) {
			if (marital.getLabel().equals(label)) {
				return marital;
			}
		}
		return null;
	}
}
