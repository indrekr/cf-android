package com.credifair.android.util.api;

import com.credifair.android.util.request.SignupRequest;

import java.util.HashMap;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by indrek.ruubel on 21/05/2016.
 */
public interface ApiCustomerService {

	@Headers({
			"Content-Type: application/x-www-form-urlencoded",
			"Accept: application/json"
	})
	@POST("/api/v1/oauth/token?grant_type=client_credentials")
	Call<HashMap<String, Object>> getClientToken(@Header("Authorization") String authorization);

	@POST("/api/v1/customer/")
	Call<HashMap<String, Object>> signup(@Header("Authorization") String authorization, @Body SignupRequest request);


	@FormUrlEncoded
	@Headers({
			"Content-Type: application/x-www-form-urlencoded",
			"Accept: application/json"
	})
	@POST("/api/v1/oauth/token")
	Call<HashMap<String, Object>> getAccessToken(@Header("Authorization") String authorization,
												 @Field("username") String username,
												 @Field("password") String password,
												 @Field("grant_type") String grantType,
												 @Field("scope") String scope);

	@GET("/api/v1/customer/getStep")
	Call<HashMap<String, Object>> getStep(@Header("Authorization") String authorization);

	@PUT("/api/v1/customer/details")
	Call<HashMap<String, Object>> putData(@Header("Authorization") String authorization, @Body HashMap<String, Object> request);

	@Multipart
	@POST("/api/v1/customer/upload")
	Call<HashMap<String, Object>> upload(@Header("Authorization") String authorization,
							  @Part MultipartBody.Part file);

	@POST("/api/v1/customer/idDocs")
	Call<HashMap<String,Object>> idDocsUploaded(@Header("Authorization") String authorization);


	@POST("/api/v1/customer/postBankDetails")
	Call<HashMap<String,Object>> postBankDetails(@Header("Authorization") String authorization, @Body HashMap<String, Object> request);

	@GET("/api/v1/customer/info")
	Call<HashMap<String,Object>> info(@Header("Authorization") String authorization);

	@POST("/api/v1/customer/requestLoan/{amount}")
	Call<HashMap<String,Object>> requestLoan(@Header("Authorization") String authorization, @Path("amount") int amount);

	@GET("/api/v1/customer/payOffDetails")
	Call<HashMap<String,Object>> payOffDetails(@Header("Authorization") String authorization);

	@GET("/api/v1/customer/payOffSchedule")
	Call<HashMap<String,Object>> payOffSchedule(@Header("Authorization") String authorization);

	@GET("/api/v1/customer/transactions")
	Call<HashMap<String,Object>> transactions(@Header("Authorization") String authorization);
}