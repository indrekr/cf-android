package com.credifair.android.util.request;

/**
 * Created by indrek.ruubel on 21/05/2016.
 */
public class SignupRequest {

	private String email;
	private String password;
	private Country country;

	public SignupRequest(String email, String password, Country country) {
		this.email = email;
		this.password = password;
		this.country = country;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	public Country getCountry() {
		return country;
	}
}
