package com.credifair.android.util;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.credifair.android.R;
import com.google.gson.internal.LinkedTreeMap;

import java.util.List;

/**
 * Created by indrek.ruubel on 28/05/2016.
 */
public class TransactionAdapter<T> extends ArrayAdapter<LinkedTreeMap<String, Object>> {

    private Context context;
    private List<LinkedTreeMap<String, Object>> transactions;
    private int layoutResourceId;

    public TransactionAdapter(Context context, int layoutResourceId, List<LinkedTreeMap<String, Object>> items) {
        super(context, layoutResourceId, items);
        this.context = context;
        this.transactions = items;
        this.layoutResourceId = layoutResourceId;
    }

    @Override
    public int getCount() {
        return transactions.size();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater)this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(layoutResourceId, parent, false);
        } else {
            row = convertView;
        }

        LinkedTreeMap<String, Object> transaction = transactions.get(position);

        // Amount
        String amountText = (String) transaction.get("amountText");
        TextView amountTextLabel = (TextView) row.findViewById(R.id.amountText);
        amountTextLabel.setText(amountText);

        // Type
        String type = (String) transaction.get("type");
        TextView transactionType = (TextView) row.findViewById(R.id.transactionType);
        transactionType.setText(type);

        // Date time
        String dateTime = (String) transaction.get("dateTimeFormatted");
        TextView dateTimeLabel = (TextView) row.findViewById(R.id.dateTime);
        dateTimeLabel.setText(dateTime);

        // Image
        ImageView image = (ImageView) row.findViewById(R.id.image);
        if (type.contains("payback")) {
            image.setImageResource(R.drawable.arrow_payback_circle);
        } else {
            image.setImageResource(R.drawable.arrow_borrow_circle);
        }

        return row;
    }

}
