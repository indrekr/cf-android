package com.credifair.android.util.request;

/**
 * Created by indrek.ruubel on 22/05/2016.
 */
public enum Education {

	PRIMARY("Primary"), BASIC("Basic"), VOCATIONAL("Vocational"), SECONDARY("Secondary"), HIGHER("Higher");

	private String label;

	Education(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public static Education findByLabel(String label) {
		for (Education education : values()) {
			if (education.getLabel().equals(label)) {
				return education;
			}
		}
		return null;
	}
}
