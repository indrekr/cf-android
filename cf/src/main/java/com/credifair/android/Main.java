package com.credifair.android;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import com.credifair.android.util.*;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.HashMap;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class Main extends ExtendedActivity {

	private RestClient restClient;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		findViewById(R.id.signup_button).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent k = new Intent(Main.this, Step.SIGNUP.getClazz());
				startActivity(k);
			}
		});

		findViewById(R.id.login_button).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent k = new Intent(Main.this, Step.LOGIN.getClazz());
				startActivity(k);
			}
		});

		restClient = new RestClient();
		getClientToken();
	}

	private void getClientToken() {
		restClient.getClientToken(new Callback<HashMap<String, Object>>() {
			@Override
			public void onResponse(Call<HashMap<String, Object>> call, Response<HashMap<String, Object>> response) {
				HashMap<String, Object> body = response.body();
				String access_token = (String) body.get("access_token");
				SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
				SharedPreferences.Editor editor = sharedPrefs.edit();
				editor.putString("clientToken", access_token);
				editor.commit();
			}

			@Override
			public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
				System.out.println("Failed client token fetch");
				System.out.println(t.getMessage());
			}
		});
	}
}
