package com.credifair.android;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.credifair.android.util.ExtendedActivity;

/**
 * Created by indrek.ruubel on 22/05/2016.
 */
public class Processing extends ExtendedActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.processing);

		Button logoutButton = (Button) findViewById(R.id.logout_button);

		logoutButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				logout(Processing.this);
			}
		});

		Button okayButton = (Button) findViewById(R.id.okayButton);

		okayButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finishAffinity();
			}
		});

	}

}
