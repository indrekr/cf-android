package com.credifair.android;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.credifair.android.util.RestClient;
import com.google.gson.internal.LinkedTreeMap;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by indrek.ruubel on 24/05/2016.
 */
public class PaybackFragment extends Fragment {

	private RestClient restClient;

	private RelativeLayout noPaymentInfoLabel;
	private RelativeLayout paymentInfo;
	private TextView amountText;
	private TextView dueDateLabel;
	private TextView transferExplanation;
	private TextView accountHolderName;
	private TextView accountDetailsLabel;
	private Button showScheduleButton;
	private TableLayout paymentList;
	private ScrollView scrollView;
	private LinearLayout layout;

	public PaybackFragment() {
		// Required empty public constructor
	}

	protected String getAccessTokenFromSharedPrefs() {
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
		return sharedPrefs.getString("accessToken", null);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		restClient = new RestClient();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.payback_fragment, container, false);
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (isVisibleToUser) {
			// called here when view visible
			getPaybackDetails();
		}
	}

	private void getPaybackDetails() {
		restClient.payoffDetails(getAccessTokenFromSharedPrefs(), new Callback<HashMap<String, Object>>() {
			@Override
			public void onResponse(Call<HashMap<String, Object>> call, Response<HashMap<String, Object>> response) {
				HashMap<String, Object> body = response.body();
				boolean paymentAvailable = (Boolean) body.get("duePaymentAvailable");
				if (paymentAvailable) {
					paymentInfo.setVisibility(View.VISIBLE);

					// Amount
					String amountTextFormatted = (String) body.get("duePaymentAmountText");
					amountText.setText(amountTextFormatted);

					// Due date
					String dueDate = (String) body.get("duePaymentTimeLeftText");
					dueDateLabel.setText(dueDate);

					// Transfer explanation text
					Double amountText = (Double) body.get("amountText");
					String currency = (String) body.get("currency");
					String explanation =
							String.format("Connect to your online bank or call your bank. Transfer exactly %s %s or more to CrediFair's UK account below by %s", amountText, currency, dueDate);
					transferExplanation.setText(explanation);

					// Account holder name
					String recipientName = (String) body.get("recipientName");
					accountHolderName.setText(recipientName);

					// Account details
					String recipientType = (String) body.get("recipientType");
					if (recipientType.equals("SORT")) {
						String ukSort = (String) body.get("recipientUkSort");
						String accountNumber = (String) body.get("recipientAccountNumber");
						accountDetailsLabel.setText(String.format("UK Sort: %s\nAccount number: %s", ukSort, accountNumber));
					} else if (recipientType.equals("IBAN")) {
						String iban = (String) body.get("recipientIban");
						accountDetailsLabel.setText(String.format("IBAN: %s", iban));
					}

				} else {
					noPaymentInfoLabel.setVisibility(View.VISIBLE);
				}
			}

			@Override
			public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
				System.out.println(t.getMessage());
			}
		});
	}

	private void fillInPaymentSchedule(HashMap<String, Object> data) {
		List<LinkedTreeMap<String, Object>> payments = (List<LinkedTreeMap<String, Object>>) data.get("payments");
		paymentList.removeAllViews();

		int screenWidth = layout.getWidth();

		int widthPerCell = screenWidth / 4;

		// Headers
		TableRow row1 = new TableRow(getContext());

		TextView head1 = new TextView(getContext());
		head1.setText("Date");
		head1.setWidth(widthPerCell);
		head1.setGravity(Gravity.CENTER);
		row1.addView(head1);

		TextView head2 = new TextView(getContext());
		head2.setText("Principal");
		head2.setWidth(widthPerCell);
		head2.setGravity(Gravity.CENTER);
		row1.addView(head2);

		TextView head3 = new TextView(getContext());
		head3.setText("Interest");
		head3.setWidth(widthPerCell);
		head3.setGravity(Gravity.CENTER);
		row1.addView(head3);

		TextView head4 = new TextView(getContext());
		head4.setText("Total");
		head4.setWidth(widthPerCell);
		head4.setGravity(Gravity.CENTER);
		row1.addView(head4);

		paymentList.addView(row1);

		for (LinkedTreeMap<String, Object> payment : payments) {
			String dateTime = (String) payment.get("dateTime");
			String principal = (String) payment.get("principal");
			String interest = (String) payment.get("interest");
			String total = (String) payment.get("total");

			TableRow row = new TableRow(getContext());
			row.setPadding(0, 10, 0, 10);

			TextView dateTimeLabel = new TextView(getContext());
			dateTimeLabel.setText(dateTime);
			dateTimeLabel.setGravity(Gravity.CENTER);
			row.addView(dateTimeLabel);

			TextView principalLabel = new TextView(getContext());
			principalLabel.setText(principal);
			principalLabel.setGravity(Gravity.CENTER);
			row.addView(principalLabel);

			TextView interestLabel = new TextView(getContext());
			interestLabel.setText(interest);
			interestLabel.setGravity(Gravity.CENTER);
			row.addView(interestLabel);

			TextView totalLabel = new TextView(getContext());
			totalLabel.setText(total);
			totalLabel.setGravity(Gravity.CENTER);
			row.addView(totalLabel);

			paymentList.addView(row);
		}
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		noPaymentInfoLabel = (RelativeLayout) view.findViewById(R.id.noPaymentInfo);
		paymentInfo = (RelativeLayout) view.findViewById(R.id.paymentInfoContainer);
		noPaymentInfoLabel.setVisibility(View.GONE);
		paymentInfo.setVisibility(View.GONE);

		amountText = (TextView) view.findViewById(R.id.amountText);
		dueDateLabel = (TextView) view.findViewById(R.id.dueDateLabel);
		transferExplanation = (TextView) view.findViewById(R.id.transferExplanation);
		accountHolderName = (TextView) view.findViewById(R.id.accountHolderNameLabel);
		accountDetailsLabel = (TextView) view.findViewById(R.id.accountDetailsLabel);
		showScheduleButton = (Button) view.findViewById(R.id.showScheduleButton);
		layout = (LinearLayout) view.findViewById(R.id.layout);
		scrollView = (ScrollView) view.findViewById(R.id.scrollView);
		paymentList = (TableLayout) view.findViewById(R.id.paymentList);

		showScheduleButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				restClient.payoffSchedule(getAccessTokenFromSharedPrefs(), new Callback<HashMap<String, Object>>() {
					@Override
					public void onResponse(Call<HashMap<String, Object>> call, Response<HashMap<String, Object>> response) {
						HashMap<String, Object> body = response.body();
						fillInPaymentSchedule(body);
					}
					@Override
					public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {

					}
				});
			}
		});

	}

}
