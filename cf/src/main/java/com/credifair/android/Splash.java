package com.credifair.android;

import android.content.Intent;
import android.os.Bundle;

import com.credifair.android.util.ExtendedActivity;
import com.credifair.android.util.RestClient;
import com.credifair.android.util.RouterService;
import com.credifair.android.util.Step;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by indrek.ruubel on 28/05/2016.
 */
public class Splash extends ExtendedActivity {

    private RestClient restClient;
    private RouterService routerService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        restClient = new RestClient();
        routerService = new RouterService();

        String accessToken = getAccessTokenFromSharedPrefs();

        if (accessToken != null) {
            restClient.getStep(accessToken, new Callback<HashMap<String, Object>>() {
                @Override
                public void onResponse(Call<HashMap<String, Object>> call, Response<HashMap<String, Object>> response) {
                    HashMap<String, Object> body = response.body();
                    String step = (String) body.get("goToStep");
                    Step stepRoute = routerService.routeFromString(step);
                    Intent k = new Intent(Splash.this, stepRoute.getClazz());
                    startActivity(k);
                    finish();
                }

                @Override
                public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
                    goToLanding();
                }
            });
        } else {
            goToLanding();
        }
    }

    private void goToLanding() {
        Intent k = new Intent(Splash.this, Main.class);
        startActivity(k);
        finish();
    }
}
