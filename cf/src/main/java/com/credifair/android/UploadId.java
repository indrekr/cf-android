package com.credifair.android;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import com.credifair.android.util.ExtendedActivity;
import com.credifair.android.util.RestClient;
import com.credifair.android.util.RouterService;
import com.credifair.android.util.Step;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by indrek.ruubel on 23/05/2016.
 */
public class UploadId extends ExtendedActivity {

	private Button takePictureButton;
	private ImageView imageView;
	private Uri file;
	private Button continueButton;

	private RestClient restClient;
	private RouterService routerService;

	private final static int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.upload_id);

		restClient = new RestClient();
		routerService = new RouterService();

		takePictureButton = (Button) findViewById(R.id.browseImage);
		imageView = (ImageView) findViewById(R.id.imageView);

		continueButton = (Button) findViewById(R.id.continueButton);
		continueButton.setEnabled(false);

		continueButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				restClient.idDocsUploaded(getAccessTokenFromSharedPrefs(), new Callback<HashMap<String, Object>>() {
					@Override
					public void onResponse(Call<HashMap<String, Object>> call, Response<HashMap<String, Object>> response) {
						HashMap<String, Object> body = response.body();
						String route = (String) body.get("goToStep");
						Step step = routerService.routeFromString(route);

						Intent k = new Intent(UploadId.this, step.getClazz());
						startActivity(k);
						finish();
					}

					@Override
					public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
						System.out.println(t.getMessage());
					}
				});
			}
		});

		takePictureButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				takePicture();
			}
		});

		if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
			takePictureButton.setEnabled(false);
			ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE }, 0);
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		if (requestCode == 0) {
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
					&& grantResults[1] == PackageManager.PERMISSION_GRANTED) {
				takePictureButton.setEnabled(true);
			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 100) {
			if (resultCode == RESULT_OK) {
				imageView.setImageURI(file);
				String accessToken = getAccessTokenFromSharedPrefs();
				restClient.upload(accessToken, this, file, new Callback<HashMap<String, Object>>() {
					@Override
					public void onResponse(Call<HashMap<String, Object>> call, Response<HashMap<String, Object>> response) {
						continueButton.setEnabled(true);
					}

					@Override
					public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
						System.out.println(t.getMessage());
						alertService.fail(UploadId.this, t.getMessage());
					}
				});
			}
		}
	}

	private static File getOutputMediaFile(){
		File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
				Environment.DIRECTORY_PICTURES), "cf");

		if (!mediaStorageDir.exists()){
			if (!mediaStorageDir.mkdirs()){
				return null;
			}
		}

		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		return new File(mediaStorageDir.getPath() + File.separator +
				"IMG_"+ timeStamp + ".jpg");
	}

	public void takePicture() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		file = Uri.fromFile(getOutputMediaFile());
		intent.putExtra(MediaStore.EXTRA_OUTPUT, file);

		startActivityForResult(intent, 100);
	}

}
