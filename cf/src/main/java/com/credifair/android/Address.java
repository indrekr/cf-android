package com.credifair.android;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.credifair.android.util.AlertService;
import com.credifair.android.util.ExtendedActivity;
import com.credifair.android.util.RestClient;
import com.credifair.android.util.RouterService;
import com.credifair.android.util.Step;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.HashMap;

/**
 * Created by indrek.ruubel on 22/05/2016.
 */
public class Address extends ExtendedActivity {

	private RestClient restClient;
	private RouterService routerService;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.address);

		restClient = new RestClient();
		routerService = new RouterService();

		final EditText addressInput = (EditText) findViewById(R.id.address);
		final EditText cityInput = (EditText) findViewById(R.id.city);
		final EditText postCodeInput = (EditText) findViewById(R.id.postCode);

		Button continueButton = (Button) findViewById(R.id.continueButton);

		continueButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String address = addressInput.getText().toString();
				String city = cityInput.getText().toString();
				String postCode = postCodeInput.getText().toString();

				HashMap<String, Object> request = new HashMap<String, Object>();

				request.put("ADDRESS", address);
				request.put("CITY", city);
				request.put("POSTAL_CODE", postCode);

				String accessToken = getAccessTokenFromSharedPrefs();

				restClient.putData(accessToken, request, new Callback<HashMap<String, Object>>() {
					@Override
					public void onResponse(Call<HashMap<String, Object>> call, Response<HashMap<String, Object>> response) {
						HashMap<String, Object> body = response.body();
						String route = (String) body.get("goToStep");
						Step step = routerService.routeFromString(route);

						Intent k = new Intent(Address.this, step.getClazz());
						startActivity(k);
						finish();
					}

					@Override
					public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
						System.out.println(t.getMessage());
						alertService.fail(Address.this, t.getMessage());
					}
				});

			}
		});

	}
}
